FROM openjdk:8

LABEL Yazeed Alkhawaldeh

EXPOSE 8090

COPY target/assignment-*.jar /usr/bin

WORKDIR /usr/bin

ENTRYPOINT java -jar -Dspring.profiles.active=h2 assignment-*.jar
